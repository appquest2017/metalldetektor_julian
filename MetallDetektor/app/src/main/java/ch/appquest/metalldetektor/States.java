package ch.appquest.metalldetektor;


import android.graphics.Color;

public class States {

    public static String getStateText(double input) {
        if (input < 50) {
            return "Kein Magnet";
        } else if (input < 260) {
            return "Fern";
        } else if (input < 500) {
            return "Nah";
        } else {
            return "Sehr nah";
        }
    }

    public static int getStateColor(double input) {
        if (input < 50) {
            return Color.parseColor("#D01919");
        } else if (input < 260) {
            return Color.parseColor("#CD8019");
        } else if (input < 500) {
            return Color.parseColor("#AFCB19");
        } else {
            return Color.parseColor("#19C91B");
        }
    }
}
