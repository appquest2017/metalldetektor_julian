package ch.appquest.metalldetektor;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager sensorManager;
    private Sensor magentic_sensor;

    private TextView lblResult, lblMaxResult, lblResultText;
    private ProgressBar proResult;

    private double highest_value = 1000, highest_value_not_default = 0;
    private static final int SCAN_QR_CODE_REQUEST_CODE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        magentic_sensor = sensorManager.getSensorList(Sensor.TYPE_MAGNETIC_FIELD).get(0);

        lblResult = (TextView) findViewById(R.id.lblResult);
        lblMaxResult = (TextView) findViewById(R.id.lblMaxResult);
        lblResultText = (TextView) findViewById(R.id.lblResultText);
        proResult = (ProgressBar) findViewById(R.id.proResult);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, magentic_sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor == magentic_sensor) {
            float[] mag = sensorEvent.values;
            double betrag = Math.sqrt(mag[0] * mag[0] + mag[1] * mag[1] + mag[2] * mag[2]);

            lblResult.setText("Derzeit: " + betrag);

            lblResultText.setText(States.getStateText(betrag));
            lblResultText.setTextColor(States.getStateColor(betrag));

            if (highest_value < betrag) {
                highest_value = betrag;
                proResult.setMax(Integer.parseInt(String.valueOf(Math.round(betrag))));
            }
            if (highest_value_not_default < betrag) {
                highest_value_not_default = betrag;
            }

            lblMaxResult.setText("Maximum: " + String.valueOf((int) Math.round(highest_value_not_default)));

            proResult.setMax((int) highest_value);
            proResult.setProgress(Integer.parseInt(String.valueOf(Math.round(betrag))));
        } else {
            Toast.makeText(this, "Wrong sensor", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem menuItem = menu.add("Log");
        menuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
                startActivityForResult(intent, SCAN_QR_CODE_REQUEST_CODE);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == SCAN_QR_CODE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String logMsg = intent.getStringExtra("SCAN_RESULT");
                try {
                    log(logMsg);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                    Toast.makeText(this, ex.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void log(String qrCode) throws JSONException {
        Intent intent = new Intent("ch.appquest.intent.LOG");

        if (getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY).isEmpty()) {
            Toast.makeText(this, "Logbook App not Installed", Toast.LENGTH_LONG).show();
            return;
        }

        JSONObject results = new JSONObject();
        results.put("task", "Metalldetektor");
        results.put("solution", qrCode);

        String logmessage = results.toString();
        intent.putExtra("ch.appquest.logmessage", logmessage);

        startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
